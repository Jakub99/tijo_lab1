package com.company;

class Girl extends Rose {

    private Flower flower;
    private short age;

    Girl() {}

    public Flower Flower() {
        return this.flower;
    }

    public void reciveFlower(Flower flower) {
        this.flower = flower;
    }

    public short Age() {
        return this.age;
    }

    public void Age(short age) {
        this.age = age;
    }
}